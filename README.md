# Simplon Trains - GeoGame

A small geographic game based on the RESTcountries API and axios.

You are on a random country and need to move to another country who is not so far.
You navigate to borders countries by clicking on their name in the list on the left.

### Install

npm install
npm run dev

