import { axios } from './index.js'

/**
 * 
 * @param {*} country BorderCountry
 */
export function getBorderCountry(country) {
    // On return deux fois, une fois la fonction pour son statut et une autre fois les datas à l'intérieur du then
    let newCountry = country.borders[Math.floor(Math.random() * country.borders.length)]
    return axios.get('https://restcountries.eu/rest/v2/alpha/' + newCountry).then(function (response) {
        return response.data;
    });
};