export const axios = require('axios');
import { displayBorders } from './setup.js';
import { getBorderCountry } from './functions.js';

let clickCount = 0;
let startingCountry;
let endingCountry;
setEnvironment();

export function addCount(currentCountry) {
    clickCount++;
    if (clickCount === 10) {
        alert('Game over');
        let clickCount = 0;
        setEnvironment();
    } else {
        if (currentCountry.name === endingCountry.name) {
            alert('You Won !')
            let clickCount = 0;
            setEnvironment();
        }
        document.querySelector('#counter').innerHTML = 'Attemps : ' + clickCount;
    }
}


async function setEnvironment() {
    document.querySelector('#target').innerHTML = '';
    document.querySelector('.cascade').innerHTML = '';
    document.querySelector('#steps').innerHTML = '';

    document.querySelector('#counter').innerHTML = 'Lets play ?';
    await axios.get('https://restcountries.eu/rest/v2/all')
        .then(async function (response) {
            startingCountry = await response.data[Math.floor(Math.random() * response.data.length)];
            if (startingCountry.borders.length !== 0) {
                displayBorders(startingCountry.name);
                let tempCountry = startingCountry;
                for (let i = 0; i < 10; i++) {
                        tempCountry = await getBorderCountry(tempCountry)
                }
                endingCountry = tempCountry;
                return await [startingCountry, endingCountry];
            } else {
                return undefined;
            }
            //Promise.all([startingCountry, tempCountry]);
        })
        .then(async (response) => {
            if (response !== undefined) {
                await console.log(response);
                let borderList = document.createElement('div');
                document.querySelector('#target').appendChild(borderList);
                let header = borderList.appendChild(document.createElement('h3'));
                header.textContent = 'Let\'s go to ' + response[1].translations.fr;
            } else {
                setEnvironment();
            }
        });
}


