import { axios } from './index.js'
import { addCount } from './index.js'


export function displayBorders(country) {
    axios.get('https://restcountries.eu/rest/v2/name/' + country).then(async function (response) {
        let borderList = document.createElement('div');
        document.querySelector('.cascade').innerHTML = '';
        document.querySelector('.cascade').appendChild(borderList);
        let header = borderList.appendChild(document.createElement('h3'));
        header.textContent = 'You are on ' + country;
        for (const iterator of response.data[0].borders) {
            //let country = getCountryByIso(iterator);
            let result = await axios.get('https://restcountries.eu/rest/v2/alpha/' + iterator).then(async function (response) {
                let div = document.createElement('div');
                let img = document.createElement('img');
                let p = document.createElement('p');

                img.src = response.data.flag;
                img.className += 'flag';

                p.textContent = response.data.translations.fr;
                div.addEventListener('click', function () {
                    addCount(response.data);
                    createCard(response.data);
                    document.querySelector('.cascade').innerHTML = '';
                    displayBorders(response.data.name);
                })
                borderList.appendChild(div);
                div.appendChild(img);
                div.appendChild(p);
            });
        }
    });
}

export function createCard(country) {
    let steps = document.querySelector('#steps');

    let div = document.createElement('div');
    let card = document.createElement('div');
    let img = document.createElement('img');
    let cardBody = document.createElement('div');
    let cardTitle = document.createElement('h5');
    let cardText = document.createElement('p');

    div.className = 'col-2';
    img.className = 'card-img-top';
    card.className = 'card';
    cardBody.className = 'card-body';
    cardTitle.className = 'card-title';
    cardText.className = 'card-text';

    img.src = country.flag;
    cardTitle.textContent = country.translations.fr;
    cardText.innerHTML = '<ul><li>Capital : ' + country.capital + '</li><li>Pop : '+country.population+'</li></ul>';

    steps.appendChild(div);
    div.appendChild(card);
    card.appendChild(img);
    card.appendChild(cardBody);
    cardBody.appendChild(cardTitle);
    cardBody.appendChild(cardText);
}